# flake8: noqa
GET_NAME_PROMPT = """
This tool will fetch name of the person with given id number. It will return the name as a string. **VERY IMPORTANT**: You must specify the id number as an integer.
"""

GET_USERS_PROMPT = """
This tool will fetch list of the user id's defined in the system. It takes no input.
"""
