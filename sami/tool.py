from typing import Optional

from langchain.callbacks.manager import CallbackManagerForToolRun
from langchain.pydantic_v1 import Field
from langchain.tools.base import BaseTool
from sami.sami import SamiAPIWrapper

class SamiAction(BaseTool):
    """Tool for interacting with the Sami API."""

    api_wrapper: SamiAPIWrapper = Field(default_factory=SamiAPIWrapper)
    mode: str
    name: str = ""
    description: str = ""

    def _run(
        self,
        instructions: str,
        run_manager: Optional[CallbackManagerForToolRun] = None,
    ) -> str:
        """Use the Sami API to run an operation."""
        return self.api_wrapper.run(self.mode, instructions)
