"""GitHub Toolkit."""
from typing import Dict, List

from langchain.agents.agent_toolkits.base import BaseToolkit
from langchain.tools import BaseTool
from sami.prompt import (
    GET_NAME_PROMPT,
    GET_USERS_PROMPT,
)
from sami.tool import SamiAction
from sami.sami import SamiAPIWrapper

class SamiToolkit(BaseToolkit):
    """Sami Toolkit."""

    tools: List[BaseTool] = []

    @classmethod
    def from_sami_api_wrapper(
        cls, sami_api_wrapper: SamiAPIWrapper
    ) -> "SamiToolkit":
        operations: List[Dict] = [
            {
                "mode": "get_name",
                "name": "Get Name",
                "description": GET_NAME_PROMPT,
            },
            {
                "mode": "get_users",
                "name": "Get Users",
                "description": GET_USERS_PROMPT,
            },
        ]
        tools = [
            SamiAction(
                name=action["name"],
                description=action["description"],
                mode=action["mode"],
                api_wrapper=sami_api_wrapper,
            )
            for action in operations
        ]
        return cls(tools=tools)

    def get_tools(self) -> List[BaseTool]:
        """Get the tools in the toolkit."""
        return self.tools
