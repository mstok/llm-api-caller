"""Util that calls sami's api."""
from __future__ import annotations

import json
from typing import TYPE_CHECKING, Any, Dict, List, Optional

from langchain.pydantic_v1 import BaseModel, Extra, root_validator
from langchain.utils import get_from_dict_or_env

class SamiAPIWrapper(BaseModel):
    """Wrapper for Sami's API."""

    sami: Any  #: :meta private:
    sami_group: Optional[str] = None
    """The name of the Sami's api group, in the form {group-name}."""

    class Config:
        """Configuration for this pydantic object."""

        extra = Extra.forbid

    @root_validator()
    def validate_environment(cls, values: Dict) -> Dict:
        """Validate that api key and python package exists in environment."""
        sami_group = get_from_dict_or_env(
            values, "sami_group", "SAMI_GROUP"
        )

        try:
            import sami.sami as sami

        except ImportError:
            raise ImportError(
                "Import failed!"
            )

        values["sami"] = "instance-placeholder"
        values["sami_group"] = sami_group

        return values

    def get_name(self, id_number: int) -> str:
        """
        Fetches name of the person with given id number
        Parameters:
            id_number(int): The id number for the sami's user
        Returns:
            str: Name of the user with given id.
        """
        # Do stuff for operation here
        return f"Name of the user with id {id_number} is Sami!"

    def get_users(self, ) -> Dict[str, str]:
        """
        Fetches list of users
        Parameters:
        Returns:
            dict: A dictionary containing the user ids and names.
        """
        # Do stuff for operation here
        return {
            "1": "Sami",
            "2": "Sami",
            "3": "Sami",
        }

    def run(self, mode: str, query: str) -> str:
        if mode == "get_users":
            return self.get_users()
        elif mode == "get_name":
            return json.dumps(self.get_name(int(query)))
        else:
            raise ValueError("Invalid mode" + mode)
