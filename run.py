#!/usr/bin/env python3
from dotenv import load_dotenv
import os

from langchain.agents import AgentType
from langchain.agents import initialize_agent
from sami.toolkit import SamiToolkit
from langchain.llms import GPT4All
from sami.sami import SamiAPIWrapper

if not load_dotenv():
    print("Could not load .env file or it is empty. Please check if it exists and is readable.")
    exit(1)

model_path = os.environ.get('MODEL_PATH')
model_n_ctx = os.environ.get('MODEL_N_CTX')
model_n_batch = int(os.environ.get('MODEL_N_BATCH',8))

def main():
    llm = GPT4All(model=model_path, max_tokens=model_n_ctx, backend='gptj', n_batch=model_n_batch, verbose=False)
    sami = SamiAPIWrapper()
    toolkit = SamiToolkit.from_sami_api_wrapper(sami)
    agent = initialize_agent(
        toolkit.get_tools(), llm, agent=AgentType.ZERO_SHOT_REACT_DESCRIPTION, verbose=True
    )
    agent.run(
        "Can you get me name of the user with id 62?"
    )

    # qa = RetrievalQA.from_chain_type(llm=llm, chain_type="stuff", retriever=retriever, return_source_documents= not args.hide_source)
    # # Interactive questions and answers
    # while True:
    #     query = input("\nEnter a query: ")
    #     if query == "exit":
    #         break
    #     if query.strip() == "":
    #         continue

    #     # Get the answer from the chain
    #     start = time.time()
    #     res = qa(query)
    #     answer, docs = res['result'], [] if args.hide_source else res['source_documents']
    #     end = time.time()

    #     # Print the result
    #     print("\n\n> Question:")
    #     print(query)
    #     print(f"\n> Answer (took {round(end - start, 2)} s.):")
    #     print(answer)

    #     # Print the relevant sources used for the answer
    #     for document in docs:
    #         print("\n> " + document.metadata["source"] + ":")
    #         print(document.page_content)

# def parse_arguments():
#     parser = argparse.ArgumentParser(description='privateGPT: Ask questions to your documents without an internet connection, '
#                                                  'using the power of LLMs.')
#     parser.add_argument("--hide-source", "-S", action='store_true',
#                         help='Use this flag to disable printing of source documents used for answers.')

#     parser.add_argument("--mute-stream", "-M",
#                         action='store_true',
#                         help='Use this flag to disable the streaming StdOut callback for LLMs.')

#     return parser.parse_args()


if __name__ == "__main__":
    main()
